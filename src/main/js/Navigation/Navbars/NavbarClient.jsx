import React from 'react';
import PropTypes from 'react';
import { Link } from 'react-router';
import { Navbar,Nav, NavItem , NavDropdown , MenuItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import UserActions from '../../Actions/UserAction.jsx';

class NavbarClient extends React.Component {
    

    myFunc(e){
      UserActions.logout();
      this.context.router.replace("/");
    }

    render() {
      return (
              <Navbar inverse collapseOnSelect fixedTop>
              <Navbar.Header>
                <Navbar.Brand>
                  <a href="#">Hell Resort</a>
                </Navbar.Brand>
                <Navbar.Toggle />
              </Navbar.Header>
              <Navbar.Collapse>
                  <Nav  bsStyle = "pills">
                      <LinkContainer to="/viewrooms">
                          <NavItem >View Rooms</NavItem>
                      </LinkContainer>
                      <LinkContainer to="/products">
                          <NavItem >Products</NavItem>
                      </LinkContainer>
                      <LinkContainer to="/orders">
                          <NavItem >Orders</NavItem>
                      </LinkContainer>
                      <LinkContainer to="/addreview">
                          <NavItem >Review</NavItem>
                      </LinkContainer>
                      <NavItem onClick={this.myFunc.bind(this)}>Logout</NavItem>
                  </Nav>
              </Navbar.Collapse>
            </Navbar>
      );
   }
}

NavbarClient.contextTypes = {
    router: function() { return React.PropTypes.func.isRequired; }
};

export default NavbarClient;
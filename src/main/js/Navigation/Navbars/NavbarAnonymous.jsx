import React from 'react';
import { Link } from 'react-router';
import { Navbar,Nav, NavItem , NavDropdown , MenuItem, Modal , Button } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import Login from './Auth/Login.jsx';

class NavbarAnonymous extends React.Component {
    
    constructor(){
      super();
      this.state={showModal:false};
    }
    openModal(){
      this.setState({ showModal: true });
    }

    closeModal() {
      this.setState({ showModal: false });
    }

    componentWillUnmount(){
      console.log("NavbarAnonymous will unmount!");
    }

    render() {
      return (
        <div>
              <Navbar inverse collapseOnSelect fixedTop>
              <Navbar.Header>
                <Navbar.Brand>
                  <a href="#">Hell Resort</a>
                </Navbar.Brand>
                <Navbar.Toggle />
              </Navbar.Header>
              <Navbar.Collapse>
                  <Nav  bsStyle = "pills">
                      <LinkContainer to="/viewrooms">
                          <NavItem >View Rooms</NavItem>
                      </LinkContainer>
                      <NavItem onClick={this.openModal.bind(this)}>Login</NavItem>
                  </Nav>
              </Navbar.Collapse>
            </Navbar>
            <Modal show={this.state.showModal} onHide={this.closeModal.bind(this)}>
               <Modal.Header closeButton>
                 <Modal.Title>Wellcome</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Login/>
              </Modal.Body>
            </Modal>
            </div>
      );
   }
}

export default NavbarAnonymous;
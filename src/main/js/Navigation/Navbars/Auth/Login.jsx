import React from 'react';
import Reflux from 'reflux';
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button, Form, Col, Modal } from 'react-bootstrap';
import { Link } from 'react-router';
import UserActions from '../../../Actions/UserAction.jsx';
class Login extends React.Component {
   
    constructor(){
        super();
        this.state={
          username:'',
          password:'',
          fullName:'',
          passre:'',
          regUI:0,
        };
    }

   onSubmitLogin(e){
    console.log("calling login action!");
    e.preventDefault();
    UserActions.login(this.state.username,this.state.password);
   }

   onSubmitRegister(e){
    e.preventDefault();
    this.setState({regUI:1});   
   }

   onRegClient(e){
    e.preventDefault();   
    var user = {};
    user.username = this.state.username;
    user.password = this.state.password;
    user.fullname = this.state.fullName;
    UserActions.AddClient(user);
   }
   
   userNameChange(e){
       this.setState({username:e.target.value});
   }
   
   passChange(e){
       this.setState({password:e.target.value});
   }

   passreChange(e){
      this.setState({passre:e.target.value});
   }

   onNameChange(e){
      this.setState({fullName:e.target.value});
   }

  cancelReg(e){
      this.setState({regUI:0});
   }
   
   getPassValidationState() {
    const length = this.state.password.length;
    if (length > 6) return 'success';
    else if (length > 3) return 'warning';
    else if (length > 0) return 'error';
  }

  validatePass(){
    if (this.state.password != '')
    return (this.state.password == this.state.passre)?'success':'error';
  }

  render() {
    if (this.state.regUI == 0){
      return (
        /*
          UI for Login

        */
            <Form horizontal>
            <FormGroup controlId="formHorizontalEmail">
              <Col componentClass={ControlLabel} sm={2}>
                Username
              </Col>
              <Col sm={6}>
                <FormControl 
                  type="text" 
                  name="username"
                  placeholder="Username"
                  value={this.state.username} 
                  onChange={this.userNameChange.bind(this)}
                />
              </Col>
               
            </FormGroup>

            <FormGroup controlId="formHorizontalPassword">
              <Col componentClass={ControlLabel} sm={2}>
                Password
              </Col>
              <Col sm={6}>
                <FormControl 
                type="password" 
                name="password"
                placeholder="Password" 
                value={this.state.password}
                onChange={this.passChange.bind(this)}
                />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col smOffset={2} sm={2}>
                <Button type="submit" bsStyle="success" onClick={this.onSubmitLogin.bind(this)}>
                  Sign in
                </Button>
              </Col>
              <Col sm={2}>
                <Button type="submit" bsStyle="primary" onClick={this.onSubmitRegister.bind(this)}>
                  Register
                </Button>
              </Col>
            </FormGroup>
          </Form>
      );
    }else{
      /*
      UI for registering.

      */
      return(
        <Form horizontal>
              <FormGroup controlId="formHorizontalUsername">
                <Col componentClass={ControlLabel} sm={3}>
                  Username
                </Col>
                <Col sm={6}>
                  <FormControl 
                    type="text" 
                    name="username"
                    placeholder="Username"
                    value={this.state.username} 
                    onChange={this.userNameChange.bind(this)}
                  />
                </Col>
              </FormGroup>

              <FormGroup controlId="formHorizontalPassword" validationState={this.getPassValidationState()}>
                <Col componentClass={ControlLabel} sm={3}>
                  Password
                </Col>
                <Col sm={6}>
                  <FormControl 
                  type="password" 
                  name="password"
                  placeholder="Password" 
                  value={this.state.password}
                  onChange={this.passChange.bind(this)}
                  />
                  <FormControl.Feedback />
                  <HelpBlock>Password needs to be at least 4 characters long!</HelpBlock>
                </Col>
              </FormGroup>

              <FormGroup controlId="formHorizontalPasswordAgain" validationState={this.validatePass()}>
                <Col componentClass={ControlLabel} sm={3}>
                  Repeat Password
                </Col>
                <Col sm={6}>
                  <FormControl 
                  type="password" 
                  name="passwordre"
                  placeholder="Password" 
                  value={this.state.passwordre}
                  onChange={this.passreChange.bind(this)}
                  />
                </Col>
              </FormGroup>

              <FormGroup controlId="formHorizontalFullName">
                <Col componentClass={ControlLabel} sm={3}>
                  Full Name
                </Col>
                <Col sm={6}>
                  <FormControl 
                  type="text" 
                  name="fullname"
                  placeholder="Full Name" 
                  value={this.state.fullName}
                  onChange={this.onNameChange.bind(this)}
                  />
                </Col>
              </FormGroup>


              <FormGroup>
                <Col smOffset={3} sm={2}>
                   <Button type="submit" bsStyle="primary" onClick={this.onRegClient.bind(this)}>
                    Register
                  </Button>
                </Col>
                <Col sm={2}>
                  <Button type="submit" bsStyle="danger" onClick={this.cancelReg.bind(this)}>
                    Cancel
                  </Button>
                </Col>
              </FormGroup>
            </Form>
          );
      }
  }
}

export default Login;
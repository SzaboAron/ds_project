import React from 'react';
import Reflux from 'reflux';
import { Link } from 'react-router';
import { Navbar,Nav, NavItem , NavDropdown , MenuItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import UserStore from '../Stores/UserStore.jsx';
import UserActions from '../Actions/UserAction.jsx';
import NavbarAnonymous from './Navbars/NavbarAnonymous.jsx';
import NavbarClient from './Navbars/NavbarClient.jsx';
import NavbarAdmin from './Navbars/NavbarAdmin.jsx';

class Navigation extends Reflux.Component {
   
    constructor(){
        super();
        this.store = UserStore;
     }

   componentDidMount(){
      UserActions.getUserAuth();
      }
    
    render() {
      if ('ROLE_ADMIN'==this.state.user.roles){
        return (
               <NavbarAdmin/>
        );
      }else if('ROLE_USER'==this.state.user.roles){
        return (
               <NavbarClient/>
        );
      }else{
        return (
               <NavbarAnonymous/>
        );
      }
   }
}

export default Navigation;
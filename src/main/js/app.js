import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './Layout.jsx';
import ManageRoom from './Pages/ManageRoom.jsx';
import Home from './Pages/Home.jsx';
import ViewRooms from './Pages/ViewRooms.jsx';
import UserActions from './Actions/UserAction.jsx';
import AddReview from './Pages/AddReview.jsx';
import {Router, Route, IndexRoute, hashHistory, browserHistory } from 'react-router';
import OrderProducts from './Pages/OrderProducts.jsx';
import ManageProduct from './Pages/ManageProducts.jsx';
import Orders 	from './Pages/Orders.jsx';

const app = document.getElementById('app');

ReactDOM.render(
<Router history={hashHistory} >
		<Route path="/" component={Layout} >
			<IndexRoute component={Home}/>
			<Route path= "manageroom" component={ManageRoom}/>
			<Route path= "viewrooms" component={ViewRooms}/>
			<Route path= "addreview" component={AddReview}/>
			<Route path= "products"	component={OrderProducts}/>
			<Route path= "manageproduct" component={ManageProduct}/>
			<Route path = "orders" component={Orders}/>
		</Route>
</Router>, 
app);
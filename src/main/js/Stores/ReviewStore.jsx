import Reflux from 'reflux';
import ReviewActions from '../Actions/ReviewActions.jsx';
import Axios from 'axios';
import request from 'superagent';

class ReviewStore extends Reflux.Store{

	constructor() {
        super();
        console.log("New ReviewStore created!");
        this.listenables = ReviewActions;
    }

    onAddReview(review){
        console.log('Sendign review to server!!')
    	request.post('/api/reviews/add').
    	field('review', review).
    	end((err,response)=>{
    		if (err){
    			console.log("Review Error: ", err);
    		}else{
    			console.log(response);
    		}
    	});
    }


}

export default ReviewStore;
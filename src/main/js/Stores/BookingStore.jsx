import Reflux from 'reflux';
import BookingActions from '../Actions/BookingActions.jsx';
import Axios from 'axios';
import request from 'superagent';

class BookingStore extends Reflux.Store{

	constructor() {
        super();
        console.log("New BookingStore created!")
        this.listenables = BookingActions;
    }

    onAddBooking(startingDate, endDate, roomNr){
    	request.post('/api/bookings/add').
    	field('startingDate', startingDate).
    	field('endDate',endDate).
    	field('roomNr',roomNr).
    	end((err,response)=>{
    		if (err){
    			console.log("BOOKING ERROR: ", err);
    		}else{
    			console.log(response);
    		}
    	});
    }


}

export default BookingStore;
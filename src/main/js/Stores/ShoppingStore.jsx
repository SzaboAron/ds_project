import Reflux from 'reflux';
import ShoppingActions from '../Actions/ShoppingActions.jsx';
import Axios from 'axios';
import request from 'superagent';

class ShoppingStore extends Reflux.Store{

	constructor() {
        super();
        console.log("New ShoppingStore created!")
        this.listenables = ShoppingActions;
        this.state = {Shoppings:[]};
    }

	onAddCart(cartid){
		console.log("Adding Cart!");
		request.post('/api/carts/add')
        .field('cartid', cartid)
        .end((err, response) => {
	      if (err) {
	        console.error(err);
	      }else{
	      	console.log("cart added!");
	     }
		});
	}

	onGetClientOrders(){
	    const getAll = "/api/carts/getall";
		Axios.get(getAll)
		.then((response) =>{
			this.setState({Shoppings:response.data});
			console.log(response.data);
			})
		.catch(function (error){
			console.log(error);
		});
	}

	onAddOrder(product,cartid,quantity){
		request.post('/api/orders/add')
        .field('productId', product.id)
        .field('cartId', cartid)
        .field('quantity',quantity)
        .end((err, response) => {
	      if (err) {
	        console.error(err);
	      }else{
	      	console.log(response);
	      }
		 });
	}

	onDeleteShopping(Shopping){
		request.post('/api/Shoppings/deletebyShoppingnr')
		.field('ShoppingNr',Shopping.ShoppingNr)
		.end((err,response)=>{
			if (err){
				console.error(err);
			}else{
				console.log(response);
				ShoppingActions.getAllShoppings();
			}
		});
	}

}

export default ShoppingStore;
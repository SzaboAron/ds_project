import Reflux from 'reflux';
import UserActions from '../Actions/UserAction.jsx';
import axios from 'axios';

class UserStore extends Reflux.Store{

	constructor() {
        super();
        console.log("New UserStore created!")
        this.listenables = UserActions;
        this.state = {user:{}};
    }

    onLogout(){
    	console.log("ARE YOU TRYING TO LEAVE?????");
    	const logouturl = "/logout";
    	axios.get(logouturl);
    	UserActions.getUserAuth();
    }

	onLogin(j_username,j_password){
		const loginUrl = "/login";
	    axios({
		  method: 'post',
		  url: loginUrl,
		  auth:{
		  	username:j_username,
		  	password:j_password
		  }
		})
		.then((response)=>{
			UserActions.getUserAuth();
		});
	}

	onGetUserAuth(){
		console.log("Getting user data!");
	    const login = "/api/clients/getcurrentuser";
		axios.get(login)
		.then((response) =>{
			console.log("User data is:",response.data);
			this.setState({user:response.data});
			})
		.catch((response)=>{
			console.log("The error is:",response);
		});
	}

	onAddClient(data){
		const url = "/api/clients/addclient";
		console.log("will register:",data);
		axios({
		  method: 'post',
		  url: url,
		  data:{
		  	userName:data.username,
		  	password:data.password,
		  	fullName:data.fullname
		  }
		})
		.then((response)=>{
			console.log("Addclient response:",response.data," Calling UserAuth!");
			UserActions.login(data.username, data.password);
		}).catch((error)=>{
			console.log("AddClient Error:",error);
		});
	}

}

export default UserStore;
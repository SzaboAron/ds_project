import Reflux from 'reflux';
import ProductActions from '../Actions/ProductActions.jsx';
import Axios from 'axios';
import request from 'superagent';

class ProductStore extends Reflux.Store{

	constructor() {
        super();
        console.log("New ProductStore created!")
        this.listenables = ProductActions;
        this.state = {Products:[]};
    }

	onCreateProduct(productName,inStock,price){
	      	request.post('/api/products/add').
	      	field('productName',productName).
	      	field('inStock',inStock).
	      	field('price',price)
	      	.end((err, response)=>{ 
		      	if (err) {
		        	console.error(err);
		      	}else{
		      		console.log("ELVILEG OK:",response);
		      		ProductActions.getAllProducts();
		      	}
		});
	}

	onGetAllProducts(){
	    const getAll = "/api/products/getall";
		Axios.get(getAll)
		.then((response) =>{
			this.setState({Products:response.data});
			})
		.catch(function (error){
			console.log(error);
		});
	}

	onDeleteProduct(Product){
		request.post('/api/products/deleteById')
		.field('ProductNr',Product.ProductNr)
		.end((err,response)=>{
			if (err){
				console.error(err);
			}else{
				console.log(response);
				ProductActions.getAllProducts();
			}
		});
	}

}

export default ProductStore;
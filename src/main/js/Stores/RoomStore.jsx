import Reflux from 'reflux';
import RoomActions from '../Actions/RoomActions.jsx';
import Axios from 'axios';
import request from 'superagent';

class RoomStore extends Reflux.Store{

	constructor() {
        super();
        console.log("New RoomStore created!")
        this.listenables = RoomActions;
        this.state = {rooms:[]};
    }

	onCreateRoom(file, roomNr,roomType,pricePerNight){
		console.log("in createroom");
		request.post('/upload')
        .field('name', file.name)
        .field('file', file)
        .end((err, response) => {
	      if (err) {
	        console.error(err);
	      }else{
	      	console.log(response.text);
	      	request.post('/api/rooms/add').
	      	send({
	      		roomNr:roomNr,
	      		roomType:roomType,
	      		pricePerNight:pricePerNight,
	      		image:response.text
	      	})
	      	.end((err, response)=>{ 
		      	if (err) {
		        	console.error(err);
		      	}else{
		      		console.log("ELVILEG OK:",response);
		      		RoomActions.getAllRooms();
		      	}
	  	    });
	     }
		});
	}

	onGetAllRooms(){
	    const getAll = "/api/rooms/getall";
		Axios.get(getAll)
		.then((response) =>{
			this.setState({rooms:response.data});
			})
		.catch(function (error){
			console.log(error);
		});
	}

	onUploadImage(file){
		console.log(file);
		request.post('/upload')
        .field('name', file.name)
        .field('file', file)
        .end((err, response) => {
	      if (err) {
	        console.error(err);
	      }else{
	      	console.log(response);
	      }
		 });
	}

	onDeleteRoom(room){
		request.post('/api/rooms/deletebyroomnr')
		.field('roomNr',room.roomNr)
		.end((err,response)=>{
			if (err){
				console.error(err);
			}else{
				console.log(response);
				RoomActions.getAllRooms();
			}
		});
	}

}

export default RoomStore;
import Reflux from 'reflux';

var UserActions = Reflux.createActions(
	[
	"login",
	"logout",
	"getUserAuth",
	"AddClient"
	]);

module.exports = UserActions;

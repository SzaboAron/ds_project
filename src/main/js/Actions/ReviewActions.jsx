import Reflux from 'reflux';

var ReviewActions = Reflux.createActions([
	"addReview"
	]);

module.exports = ReviewActions;
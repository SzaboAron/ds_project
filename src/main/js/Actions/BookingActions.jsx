import Reflux from 'reflux';

var BookingActions = Reflux.createActions([
	"addBooking"
	]);

module.exports = BookingActions;

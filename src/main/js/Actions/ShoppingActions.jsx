import Reflux from 'reflux';

var ShoppingActions = Reflux.createActions(
	[
	"addOrder",
	"addCart",
	"getClientOrders"
	]);

module.exports = ShoppingActions;

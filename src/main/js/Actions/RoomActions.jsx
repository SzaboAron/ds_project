import Reflux from 'reflux';

var RoomActions = Reflux.createActions([
	"createRoom",
	"getAllRooms",
	"uploadImage",
	"deleteRoom"
	]);

module.exports = RoomActions;

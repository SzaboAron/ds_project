import Reflux from 'reflux';

var ProductActions = Reflux.createActions([
  "createProduct",
  "getAllProducts",
  "deleteProduct"
  ]);

module.exports = ProductActions;

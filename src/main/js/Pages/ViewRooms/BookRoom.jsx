import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {Button, Col, Row } from 'react-bootstrap';
import 'react-datepicker/dist/react-datepicker.css';

class BookRoom extends React.Component{

	constructor(){
		super();
		this.state={
			startDate:moment(),
			endDate:moment()
		}
	}

	handleChangeStart(date){
		this.setState({startDate:date});
	}

	handleChangeEnd(date){
		this.setState({endDate:date});
	}

	render(){
		if (this.props.show==true){
			return(
				<div>
				<Col>
					<h3>Starting Date: </h3>
					<DatePicker
						inline
						selected={this.state.startDate}
				        selectsStart  startDate={this.state.startDate}
						endDate={this.state.endDate}
				        onChange={this.handleChangeStart.bind(this)} 
				     />
				 </Col>
				 <Col>
				 	<h3>End Date</h3>
				     <DatePicker
				     	inline
						selected={this.state.endDate}
						selectsEnd  startDate={this.state.startDate}
						endDate={this.state.endDate}
						onChange={this.handleChangeEnd.bind(this)}
					/>
				</Col>
				<Col>
					<Button bsStyle='success' onClick={this.props.onBook.bind(this,this.state.startDate,this.state.endDate)}>Book</Button>
				</Col>
				</div>
			);
		}else{
			return null;	

		}
	}
}

export default BookRoom;
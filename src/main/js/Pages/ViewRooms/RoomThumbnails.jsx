import React from 'react';
import { Button ,Image, Carousel, Col, Row, Thumbnail, Grid} from 'react-bootstrap';

class RoomThumbnails extends React.Component {

  render() {
    if (this.props.show==true){
      if (this.props.rooms.length > 0){
        var thumbnails = this.props.rooms.map((room)=>{
          return(
            
                <Col xs={6} sm={4} lg={3} smPull lgPull xsPull>
                    <Thumbnail src={'/images/'+room.image} alt="242x200">
                      <h3>{room.roomType} </h3> 
                      <p>Book this room for {room.pricePerNight}$/night</p>
                      <p>
                        <Button bsStyle="success" onClick={this.props.bookRoom.bind(this,room)}>
                          Book it!
                        </Button>&nbsp;
                      </p>
                   </Thumbnail>
                </Col>
              
              
             
            );
        });

        return (
          <Grid fluid >
            <Row>
             {thumbnails}
            </Row>
          </Grid>
        );
      }else{
        return(
            <div>
           <h3>No rooms are available, sorry!</h3>
          </div>
          );
      }}else{
        return null;
      }


  }
}

export default RoomThumbnails;
import React from 'react';
import { Well } from 'react-bootstrap';

class Home extends React.Component {
    
  render() {
    return (
      <Well>This is the Home page!!</Well>
    );
  }
}

export default Home;
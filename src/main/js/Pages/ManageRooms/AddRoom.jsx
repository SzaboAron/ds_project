import React from 'react';
import PropTypes from 'react';
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button, Col, Row} from 'react-bootstrap';
import RoomActions from '../../Actions/RoomActions.jsx';


class AddRoom extends React.Component {
  constructor(){
    super();
    this.state = {
      roomNr:null,
      roomType:'',
      pricePerNight:'',
      image:null,
      show:false
    };
  }
  
  onRoomNrChange(e){
    this.setState({roomNr:e.target.value});
  }

  onRoomTypeChange(e){
    this.setState({roomType:e.target.value});
  }

  onPricePerNightChange(e){
    this.setState({pricePerNight:e.target.value});
  }

  onImageChange(e){
    this.setState({image:e.target.files[0]})
    console.log(e.target.value);
  }

  updateImage(e){
    e.preventDefault();
    RoomActions.createRoom(this.state.image, this.state.roomNr, this.state.roomType,this.state.pricePerNight);
    this.setState({image:null,roomNr:'',roomType:'',pricePerNight:'',show:false});
  }

  editRoom(room,event){
    console.log(room);
  }

  deleteRoom(room,event){
    console.log(room);
    RoomActions.deleteRoom(room);

  }

  showUi(e){
    e.preventDefault();
    this.setState({show:true});
  }

  hideUi(e){
    e.preventDefault();
     this.setState({image:null,roomNr:'',roomType:'',pricePerNight:'',show:false});
  }

  render() {
    if (this.props.show == 1){
      return (
           <div>
          <Col>
            <h1>Enter Room Details:</h1>
          </Col>
           <form>
             <FormGroup controlId="formBasicText">
            <Row>
            <Col componentClass={ControlLabel} sm={1}>
                Room Number
            </Col>
            <Col sm={5} >
               <FormControl
                 type="text"
                 placeholder="Room Nr"
                 name="roomNr"
                 value={this.state.roomNr}
                 onChange={this.onRoomNrChange.bind(this)}
               />
            </Col>
            </Row>
            <Row>
            <Col componentClass={ControlLabel} sm={1}>
                Room Type
            </Col>
            <Col sm={5}>
               <FormControl
                 type="text"
                 placeholder="Room Type"
                 name="roomType"
                 value={this.state.roomType}
                 onChange={this.onRoomTypeChange.bind(this)}
               />
            </Col>
            </Row>
            <Row>
            <Col componentClass={ControlLabel} sm={1}>
                Price/Night
            </Col>
            <Col sm={5}>
               <FormControl
                 type="text"
                 placeholder="Price / Night"
                 name="pricePerNight"
                 value={this.state.pricePerNight}
                 onChange={this.onPricePerNightChange.bind(this)}
               />
            </Col>
            </Row>
            <Row>
            <Col componentClass={ControlLabel} sm={1}>
                Room Image
            </Col>
            <Col sm={5}>
           <FormControl
              id="formControlsFile"
              type="file"
              label="File"
              help="Example block-level help text here."
              value={this.state.image}
              onChange={this.onImageChange.bind(this)}
            />
            </Col>
            </Row>
               <FormControl.Feedback />
          </FormGroup>  
          <Col smOffset={1}>
          <Button type="submit" bsStyle='success' onClick={this.updateImage.bind(this)}>
           Submit
          </Button>
          <Button type="submit" bsStyle='danger' onClick={this.props.hideUi}>
           Cancel
          </Button>
           </Col>
           </form>
           </div>
      );
    }else if (this.props.show==0){
        return (
         <Button type="submit" bsStyle='success' onClick={this.props.showUi}>
           Add Room
         </Button>
          );
      }else return null;
  }

}


export default AddRoom;
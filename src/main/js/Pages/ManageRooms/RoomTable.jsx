import React from 'react';
import PropTypes from 'react';
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button, Col, Row} from 'react-bootstrap';

class RoomTable extends React.Component{

  render(){
    var roomtables = this.props.rooms.map((room)=>{
      return(
        <tr>
            <td>{room.id}</td>
            <td>{room.roomNr}</td>
            <td>{room.roomType}</td>
            <td>{room.pricePerNight}</td>
            <td>
              <Button type="submit" bsStyle="success" onClick={this.props.editRoom.bind(this,room)} >
                Edit
              </Button>
            </td>
            <td>
              <Button type="submit" bsStyle="danger" onClick={this.props.deleteRoom.bind(this,room)} >
                Delete
              </Button>
            </td>
          </tr>
          );
    });
    if (this.props.show==0){
    return (
      <div>
          <table class="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Room Number</th>
              <th>Room Type</th>
              <th>Price</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
            </thead>
            <tbody>
              {roomtables}
            </tbody>
          </table>
      </div>
      );}else{

      return null;
    }
  }

}

export default RoomTable;
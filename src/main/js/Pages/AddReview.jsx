import React from 'react';
import Reflux from 'reflux';
import ReviewStore from '../Stores/ReviewStore.jsx';
import ReviewActions from '../Actions/ReviewActions.jsx';
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button, Col, Row,Well} from 'react-bootstrap';


class AddReview extends React.Component {
 
    constructor(){
        super();
        this.state = {review:''};
        var mySingleton = Reflux.initializeGlobalStore(ReviewStore);
    }
    
    onChange(e){
        this.setState({review: e.target.value});
    }
    
	handleSubmit(e){
		e.preventDefault();
		console.log(this.state.review);
    ReviewActions.addReview(this.state.review);
	}

  render() {
    return (
          <Well>
           <form>
           <FormGroup controlId="formControlsTextarea">
		      <ControlLabel>Review our services:</ControlLabel>
		      <FormControl 
		          componentClass="textarea" 
		          placeholder="textarea" 
		          value={this.state.review}
	              onChange={this.onChange.bind(this)}
		          />
		      <Button Button type="submit" bsStyle='success' onClick={this.handleSubmit.bind(this)}>Submit</Button>
		    </FormGroup>
		    </form>
          </Well>  
     
    );
  }
};

export default AddReview;
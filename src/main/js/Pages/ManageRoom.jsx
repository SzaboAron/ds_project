import React from 'react';
import PropTypes from 'react';
import Reflux from 'reflux';
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button, Col, Row} from 'react-bootstrap';
import { Link } from 'react-router';
import UserStore from '../Stores/UserStore.jsx';
import RoomActions from '../Actions/RoomActions.jsx';
import RoomStore from '../Stores/RoomStore.jsx';
import AddRoom from './ManageRooms/AddRoom.jsx';
import RoomTable from './ManageRooms/RoomTable.jsx';
import EditRoom from './ManageRooms/EditRoom.jsx'

class ManageRoom extends Reflux.Component {
  constructor(){
    super();
    this.state = {
      auth:['ROLE_ADMIN'],
      noAuthUrl:"/",
      roomNr:null,
      roomType:'',
      pricePerNight:'',
      image:null,
      ui:0,
      editRoom:null
    };
    this.stores = [UserStore,RoomStore];
    var mySingleton = Reflux.initializeGlobalStore(RoomStore);
  }
  
  componentDidMount(){
      this.authenticate();
      RoomActions.getAllRooms();
  }

  authenticate(){
    var  authcheck= false;
    this.state.auth.map((role)=>{
      if (role == this.state.user.roles){
          authcheck = true;
      }
    });
    if (authcheck == false){
        this.context.router.push(this.state.noAuthUrl);
        }
  }

  onRoomNrChange(e){
    this.setState({roomNr:e.target.value});
  }

  onRoomTypeChange(e){
    this.setState({roomType:e.target.value});
  }

  onPricePerNightChange(e){
    this.setState({pricePerNight:e.target.value});
  }

  onImageChange(e){
    this.setState({image:e.target.files[0]})
    console.log(e.target.value);
  }

  updateImage(e){
    e.preventDefault();
    RoomActions.createRoom(this.state.image, this.state.roomNr, this.state.roomType,this.state.pricePerNight);
    this.setState({image:null,roomNr:'',roomType:'',pricePerNight:''});
  }

  editRoom(room,event){
    console.log("EDITING ROOM:",room);
    this.setState({editRoom:room,ui:2}); 
  }

  deleteRoom(room,event){
    console.log("DELETING ROOM:",room);
    RoomActions.deleteRoom(room);

  }

  hideUi(e){
    e.preventDefault();
    this.setState({ui:0});
  }

  showAddUi(e){
    e.preventDefault();
    this.setState({ui:1});
  }

  render() {
    return (
      <div>
          <RoomTable 
            show={this.state.ui}
            rooms={this.state.rooms} 
            editRoom={this.editRoom.bind(this)} 
            deleteRoom={this.deleteRoom.bind(this)} 
          />
          <AddRoom 
            show={this.state.ui} 
            hideUi={this.hideUi.bind(this)}
            showUi={this.showAddUi.bind(this)}
          />
          <EditRoom
            show={this.state.ui}
            hideUi={this.hideUi.bind(this)}
            room={this.state.editRoom}
          />
      </div>
      );
  }
}

ManageRoom.contextTypes = {
    router: function() { return React.PropTypes.func.isRequired; }
};

export default ManageRoom;
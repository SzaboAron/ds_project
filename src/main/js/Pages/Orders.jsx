import React from 'react';
import PropTypes from 'react';
import Reflux from 'reflux';
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button, Col, Row, Jumbotron} from 'react-bootstrap';
import { Link } from 'react-router';
import UserStore from '../Stores/UserStore.jsx';
import ShoppingActions from '../Actions/ShoppingActions.jsx';
import ShoppingStore from '../Stores/ShoppingStore.jsx';
import ProductTableClient from './Products/ProductTableClient.jsx';

class Orders extends Reflux.Component {
  constructor(){
    super();
    this.state = {
      auth:['ROLE_USER','ROLE_ADMIN'],
      noAuthUrl:"/",
      ProductName:null,
      inStock:'',
      price:'',
      ui:0,
      editProduct:null
    };
    this.stores = [UserStore,ShoppingStore];
    var mySingleton = Reflux.initializeGlobalStore(ShoppingStore);
  }
  
  componentDidMount(){
      this.authenticate();
      ShoppingActions.getClientOrders();
  }

  authenticate(){
    var  authcheck= false;
    this.state.auth.map((role)=>{
      if (role == this.state.user.roles){
          authcheck = true;
      }
    });
    if (authcheck == false){
        this.context.router.push(this.state.noAuthUrl);
        }
  }


  render() {
    var carts = this.state.Shoppings.map((cart)=>{
      
      var price = 0;

      var orders = cart.orders.map((order)=>{
          price = price + (order.quantity * order.product.price);    
          return(
            
                <tr>
                  <td>{order.product.productName}</td>
                  <td>${order.product.price}</td>
                  <td>{order.quantity}</td>
                </tr>
            
            );    
      });

      return(
        <Jumbotron>
        <table class='table'>
        <thead>
          <tr>
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>
              {orders}
        </tbody>
        </table>
        Total price: ${price} 
        </Jumbotron>
        );
    });
    return (
      <div>
          {carts}
      </div>
      );
  }
}

Orders.contextTypes = {
    router: function() { return React.PropTypes.func.isRequired; }
};

export default Orders;
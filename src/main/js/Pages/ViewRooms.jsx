import React from 'react';
import Reflux from 'reflux';
var RoomActions = require('../Actions/RoomActions.jsx');
import RoomStore from '../Stores/RoomStore.jsx';
import { Button ,Image, Carousel, Col, Row, Thumbnail, Grid} from 'react-bootstrap';
import BookRoom from './ViewRooms/BookRoom.jsx';
import UserStore from '../Stores/UserStore.jsx';
import UserActions from '../Actions/UserAction.jsx';
import RoomThumbnails from './ViewRooms/RoomThumbnails.jsx';
import moment from 'moment';
import BookingActions from '../Actions/BookingActions.jsx';
import BookingStore from '../Stores/BookingStore.jsx';

class ViewRooms extends Reflux.Component {

  constructor(props){
    super(props);
    this.state={
      selectedRoom:null,
      selected:false
    };
    this.stores = [RoomStore,UserStore];
    var mySingleton = Reflux.initializeGlobalStore(BookingStore);
  }

  getRooms(e){
    RoomActions.getAllRooms();
  }

  componentDidMount(){
    console.log("ViewRooms will mount!!");
    RoomActions.getAllRooms();
  }

  bookRoom(room,event){
    console.log("BOOKING: ",room);

    this.setState({selected:true, selectedRoom:room});
  }

  onBook(startDate,endDate,event){
    event.preventDefault();
    console.log("Booking for:",startDate.format("YYYY-MM-DD")," -> ",endDate.format("YYYY-MM-DD"));
    BookingActions.addBooking(startDate.format("YYYY-MM-DD"),endDate.format("YYYY-MM-DD"),this.state.selectedRoom.roomNr);
  }

  render() {
      return (
        <div>
          <RoomThumbnails
            rooms={this.state.rooms}
            bookRoom={this.bookRoom.bind(this)}
            show={!this.state.selected}
          />
          <BookRoom 
            show={this.state.selected}
            onBook={this.onBook.bind(this)}
          />
      </div>
      );
  }
}

export default ViewRooms;
import React from 'react';
import PropTypes from 'react';
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button, Col, Row} from 'react-bootstrap';
import ProductActions from '../../Actions/ProductActions.jsx';


class AddProduct extends React.Component {
  constructor(){
    super();
    this.state = {
      ProductName:'',
      inStock:'',
      Price:'',
      image:null,
      show:false
    };
  }
  
  onProductNameChange(e){
    this.setState({ProductName:e.target.value});
  }

  oninStockChange(e){
    this.setState({inStock:e.target.value});
  }

  onPriceChange(e){
    this.setState({Price:e.target.value});
  }

  updateImage(e){
    e.preventDefault();
    ProductActions.createProduct(this.state.ProductName, this.state.inStock,this.state.Price);
    this.setState({ProductName:'',inStock:'',Price:'',show:false});
  }

  editProduct(Product,event){
    console.log(Product);
  }

  deleteProduct(Product,event){
    console.log(Product);
    ProductActions.deleteProduct(Product);

  }

  showUi(e){
    e.preventDefault();
    this.setState({show:true});
  }

  hideUi(e){
    e.preventDefault();
     this.setState({image:null,ProductName:'',inStock:'',Price:'',show:false});
  }

  render() {
    if (this.props.show == 1){
      return (
           <div>
          <Col>
            <h1>Enter Product Details:</h1>
          </Col>
           <form>
             <FormGroup controlId="formBasicText">
            <Row>
            <Col componentClass={ControlLabel} sm={1}>
                Product Name
            </Col>
            <Col sm={5} >
               <FormControl
                 type="text"
                 placeholder="Product Name"
                 name="ProductName"
                 value={this.state.ProductName}
                 onChange={this.onProductNameChange.bind(this)}
               />
            </Col>
            </Row>
            <Row>
            <Col componentClass={ControlLabel} sm={1}>
                Stock
            </Col>
            <Col sm={5}>
               <FormControl
                 type="text"
                 placeholder="Stock"
                 name="inStock"
                 value={this.state.inStock}
                 onChange={this.oninStockChange.bind(this)}
               />
            </Col>
            </Row>
            <Row>
            <Col componentClass={ControlLabel} sm={1}>
                Price
            </Col>
            <Col sm={5}>
               <FormControl
                 type="text"
                 placeholder="Price"
                 name="Price"
                 value={this.state.Price}
                 onChange={this.onPriceChange.bind(this)}
               />
            </Col>
            </Row>
               <FormControl.Feedback />
          </FormGroup>  
          <Col smOffset={1}>
          <Button type="submit" bsStyle='success' onClick={this.updateImage.bind(this)}>
           Submit
          </Button>
          <Button type="submit" bsStyle='danger' onClick={this.props.hideUi}>
           Cancel
          </Button>
           </Col>
           </form>
           </div>
      );
    }else if (this.props.show==0){
        return (
         <Button type="submit" bsStyle='success' onClick={this.props.showUi}>
           Add Product
         </Button>
          );
      }else return null;
  }

}


export default AddProduct;
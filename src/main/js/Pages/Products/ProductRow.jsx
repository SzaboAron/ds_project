import React from 'react';
import Reflux from 'reflux';
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button, Col, Row, Form} from 'react-bootstrap';
import ShoppingActions from '../../Actions/ShoppingActions.jsx';

class ProductRow extends React.Component{

  constructor(){
    super();
    this.state = {quantity:0};
  }

  handleChange(e){
    this.setState({quantity:e.target.value});
  }

  Order(cartid){
    console.log("Ordering: ",this.state.quantity," of ",this.props.product.productName," for cart:",cartid);
    if (this.state.quantity > 0)
      ShoppingActions.addOrder(this.props.product,cartid,this.state.quantity);
  }

  render(){
      return(
        <tr>
            <td>{this.props.product.productName}</td>
            <td>{this.props.product.inStock}</td>
            <td>{this.props.product.price}</td>
            <td>
              <Form inline>
                <FormControl 
                  type="text" 
                  placeholder="Quantity" 
                  value={this.state.quantity}
                  onChange={this.handleChange.bind(this)}
                  />
              </Form>
            </td>
        </tr>
      );
  }

}

export default ProductRow;
import React from 'react';
import Reflux from 'reflux'
import PropTypes from 'react';
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button, Col, Row, Form} from 'react-bootstrap';
import ProductRow from './ProductRow.jsx';
import ShoppingActions from '../../Actions/ShoppingActions.jsx';
import ShoppingStore from '../../Stores/ShoppingStore.jsx';

class ProductTableClient extends React.Component{
  constructor(){
    super();
    var mySingleton = Reflux.initializeGlobalStore(ShoppingStore);
  }

  OrderAll(){
     console.log("refs are:",this.refs);
     const cartid = Date.now();
     ShoppingActions.addCart(cartid);
     
     setTimeout(()=>{
        Object.keys(this.refs).forEach((ref)=>{
         this.refs[ref].Order(cartid);
        });
     }, 2000);
     
    
  }

  render(){
    var producttables = this.props.Products.map((product)=>{
      return(
              <ProductRow product={product} ref={product.productName}/>
          );
    });
    if (this.props.show==0){
    return (
      <div>
          <table class="table">
          <thead>
            <tr>
              <th>product Name</th>
              <th>In stock</th>
              <th>Price</th>
              <th>Quantity</th>
            </tr>
            </thead>
            <tbody>
              {producttables}
            </tbody>
          </table>
          <Button type="submit" bsStyle="success" onClick={this.OrderAll.bind(this)}>
              Order
          </Button>
      </div>
      );}else{

      return null;
    }
  }

}

export default ProductTableClient;
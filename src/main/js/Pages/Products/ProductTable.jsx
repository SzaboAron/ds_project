import React from 'react';
import PropTypes from 'react';
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button, Col, Row} from 'react-bootstrap';

class ProductTable extends React.Component{

  render(){
    var producttables = this.props.Products.map((product)=>{
      return(
        <tr>
            <td>{product.id}</td>
            <td>{product.productName}</td>
            <td>{product.inStock}</td>
            <td>{product.price}</td>
            <td>
              <Button type="submit" bsStyle="danger" onClick={this.props.deleteProduct.bind(this,product)} >
                Delete
              </Button>
            </td>
          </tr>
          );
    });
    if (this.props.show==0){
    return (
      <div>
          <table class="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>product Name</th>
              <th>In stock</th>
              <th>Price</th>
              <th>Delete</th>
            </tr>
            </thead>
            <tbody>
              {producttables}
            </tbody>
          </table>
      </div>
      );}else{

      return null;
    }
  }

}

export default ProductTable;
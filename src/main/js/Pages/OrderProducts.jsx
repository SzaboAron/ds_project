import React from 'react';
import PropTypes from 'react';
import Reflux from 'reflux';
import {FormGroup, ControlLabel, FormControl, HelpBlock, Button, Col, Row} from 'react-bootstrap';
import { Link } from 'react-router';
import UserStore from '../Stores/UserStore.jsx';
import ProductActions from '../Actions/ProductActions.jsx';
import ProductStore from '../Stores/ProductStore.jsx';
import ProductTableClient from './Products/ProductTableClient.jsx';

class OrderProduct extends Reflux.Component {
  constructor(){
    super();
    this.state = {
      auth:['ROLE_USER'],
      noAuthUrl:"/",
      ProductName:null,
      inStock:'',
      price:'',
      ui:0,
      editProduct:null
    };
    this.stores = [UserStore,ProductStore];
    var mySingleton = Reflux.initializeGlobalStore(ProductStore);
  }
  
  componentDidMount(){
      this.authenticate();
      ProductActions.getAllProducts();
  }

  authenticate(){
    var  authcheck= false;
    this.state.auth.map((role)=>{
      if (role == this.state.user.roles){
          authcheck = true;
      }
    });
    if (authcheck == false){
        this.context.router.push(this.state.noAuthUrl);
        }
  }

  onProductNameChange(e){
    this.setState({ProductName:e.target.value});
  }

  onProductTypeChange(e){
    this.setState({ProductType:e.target.value});
  }

  onPricePerNightChange(e){
    this.setState({pricePerNight:e.target.value});
  }

  deleteProduct(Product,event){
    console.log("DELETING Product:",Product);
    ProductActions.deleteProduct(Product);

  }

  hideUi(e){
    e.preventDefault();
    this.setState({ui:0});
  }

  showAddUi(e){
    e.preventDefault();
    this.setState({ui:1});
  }

  render() {
    return (
      <div>
          <ProductTableClient 
            show={this.state.ui}
            Products={this.state.Products} 
            deleteProduct={this.deleteProduct.bind(this)} 
          />
      </div>
      );
  }
}

OrderProduct.contextTypes = {
    router: function() { return React.PropTypes.func.isRequired; }
};

export default OrderProduct;
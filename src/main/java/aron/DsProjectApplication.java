package aron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@ComponentScan({"security","controllers","services"})
@EnableJpaRepositories("repositories")
@EntityScan("entities")
@EnableTransactionManagement
public class DsProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DsProjectApplication.class, args);
	}
}

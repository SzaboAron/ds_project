package entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "bookings")
public class Booking {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idbookings;
	@ManyToOne
	@JoinColumn(name="client_id")
	private User client;
	@ManyToOne
	@JoinColumn(name="room_id")
	private Room room;
	@Column(columnDefinition="DATE")
	@Temporal(TemporalType.DATE)
	private Date startingDate;
	@Column(columnDefinition="DATE")
	@Temporal(TemporalType.DATE)
	private Date endDate;
	
	public Booking() {
		super();
	}

	public Booking(User client, Room room, Date startingDate, Date endDate) {
		super();
		this.client = client;
		this.room = room;
		this.startingDate = startingDate;
		this.endDate = endDate;
	}

	public Long getIdbookings() {
		return idbookings;
	}

	public User getClinet() {
		return client;
	}

	public void setClinet(User client) {
		this.client = client;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Date getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
	
	
}

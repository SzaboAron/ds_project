package entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="shopping_cart")
public class ShoppingCart {
	
	@Id
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="client_id")
	private User user;

	@OneToMany(mappedBy = "shoppingCart")
	private List<Order> orders;

	public ShoppingCart(Long id, User user, List<Order> orders) {
		super();
		this.id = id;
		this.user = user;
		this.orders = orders;
	}

	public ShoppingCart() {
		super();
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	
	
	
}

package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="rooms")
public class Room {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idrooms;
	private int roomNr;
	private String roomType;
	private float pricePerNight;
	private String image;
	
	public Room() {
		super();
	}
	
	public Room(int roomNr, String roomType, float pricePerNight) {
		super();
		this.roomNr = roomNr;
		this.roomType = roomType;
		this.pricePerNight = pricePerNight;
	}
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getId() {
		return idrooms;
	}
	public int getRoomNr() {
		return roomNr;
	}
	public void setRoomNr(int roomNr) {
		this.roomNr = roomNr;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public float getPricePerNight() {
		return pricePerNight;
	}
	public void setPricePerNight(float pricePerNight) {
		this.pricePerNight = pricePerNight;
	}

	@Override
	public String toString() {
		return "Room [idrooms=" + idrooms + ", roomNr=" + roomNr + ", roomType=" + roomType + ", pricePerNight="
				+ pricePerNight + "]";
	}
	
}

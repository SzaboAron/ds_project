package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="reviews")
public class Review {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idreviews;
	@ManyToOne
	@JoinColumn(name="client_id")
	private User client;
	private String review;
		
	public Review() {
		super();
	}

	public Review(User client, String review) {
		super();
		this.client = client;
		this.review = review;
	}

	public Long getIdreviews() {
		return idreviews;
	}

	public User getClient() {
		return client;
	}

	public void setClient(User client) {
		this.client = client;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}
		
}

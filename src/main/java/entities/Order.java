package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idorders;
	
	@OneToOne
	@JoinColumn(name="product_id")
	private Product product;
	
	private int quantity;
	
	@ManyToOne
	@JoinColumn(name="cart_id")
	@JsonIgnore //to prevent infinite loop
	private ShoppingCart shoppingCart;
	
	public Order() {
		super();
	}

	public Order(Product product, int quantity, ShoppingCart shoppingCart) {
		super();
		this.product = product;
		this.quantity = quantity;
		this.shoppingCart = shoppingCart;
	}

	public Long getIdorders() {
		return idorders;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public ShoppingCart getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ShoppingCart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	
	
	
	
}

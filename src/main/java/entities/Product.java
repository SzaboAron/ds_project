package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Product {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idproducts;
	private String productName;
	private int inStock;
	private float price;
	
	public Product() {
		super();
	}

	public Product(Long idproducts, String productName, int inStock, float price) {
		super();
		this.idproducts = idproducts;
		this.productName = productName;
		this.inStock = inStock;
		this.price = price;
	}

	public Long getId() {
		return idproducts;
	}

	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getInStock() {
		return inStock;
	}
	public void setInStock(int inStock) {
		this.inStock = inStock;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	
	
}

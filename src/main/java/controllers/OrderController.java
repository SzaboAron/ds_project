package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Order;
import services.OrderService;

@RestController
@RequestMapping("/api/orders/")
public class OrderController {

	private OrderService orderService;
	
	@Autowired
	public OrderController(OrderService orderService){
		this.orderService = orderService;
	}
	
	/**
	 * Methods for the API
	 */
	
	@ResponseBody
	@RequestMapping(value = "/getall", method=RequestMethod.GET)
	public List<Order> getAll(){
		return orderService.getAllOrders();
	}
	
	@ResponseBody
	@RequestMapping(value = "/add", method=RequestMethod.POST)
	public boolean addOrder(@RequestParam Long productId, @RequestParam Long cartId, @RequestParam int quantity){
		orderService.addOrder(productId,cartId,quantity);
		return true;
	}
}

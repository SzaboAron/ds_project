package controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController {

    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody String handleFileUpload(@RequestParam String name, @RequestParam MultipartFile file){
    	if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                File myFile= new File("images/"+name);
                System.out.println(myFile.getAbsolutePath());
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(myFile));
                stream.write(bytes);
                stream.close();
                System.out.println("file saved somewhere!");
                return  name;
            } catch (Exception e) {
                return null;
            }
        } else {
        	System.out.println("Empty File!!");
            return null;
        }
    }

}

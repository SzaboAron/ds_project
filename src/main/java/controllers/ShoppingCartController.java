package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.ShoppingCart;
import entities.User;
import services.ShoppingCartService;

@RestController
@RequestMapping("/api/carts")
public class ShoppingCartController {

	private ShoppingCartService shoppingCartService;
	
	@Autowired
	public ShoppingCartController(ShoppingCartService shoppingCartService){
		this.shoppingCartService = shoppingCartService;
	}
	
	@ResponseBody
	@RequestMapping(value = "/add", method=RequestMethod.POST)
	public ShoppingCart addCart(@RequestParam Long cartid){
		ShoppingCart cart = new ShoppingCart();
		cart.setId(cartid);
		return shoppingCartService.addCart(cart);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getall", method=RequestMethod.GET)
	public List<ShoppingCart> getUsers(){
		return shoppingCartService.getAllCarts();
	}
	
	@ResponseBody
	@RequestMapping(value = "/getabyuser", method=RequestMethod.GET)
	public ShoppingCart getByUser(){
		return shoppingCartService.getByClient();
	}
	
	
}

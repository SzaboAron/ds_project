package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.User;
import services.UserService;

@RestController
@RequestMapping("/api/clients")
public class ClientController {
	
	private UserService userService;
	
	@Autowired
	public ClientController(UserService userService){
		this.userService = userService;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getall", method=RequestMethod.GET)
	public List<User> getUsers(){
		return userService.getAllUsers();
	}
	
	@ResponseBody
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public User addUser(User user){
		userService.addUser(user);
		return user;
	}
	
	@ResponseBody
	@RequestMapping(value="/addclient",method=RequestMethod.POST,  headers = "Content-Type=application/json")
	public User addClient(@RequestBody User user){
		return userService.addClient(user);
	}
	
	@ResponseBody
	@RequestMapping(value="/getbyname", method=RequestMethod.GET)
	public User geByName(User user){
		User temp = userService.findByUserName(user);
		return temp;
	}
	
	@ResponseBody
	@RequestMapping(value="/getcurrentuser", method=RequestMethod.GET)
	public User getByName(){
		User temp = userService.getCurrentUser();
		return temp;
	}
	
	@ResponseBody
	@RequestMapping(value="/deletebyusername", method=RequestMethod.GET)
	public User deleteByUserName(User user){
		return userService.deleteByUsername(user);
	}
	
	@ResponseBody
	@RequestMapping(value="/check", method=RequestMethod.GET)
	public boolean checkUserName(User user){
		return userService.checkUserName(user);
	}
	/**
	 * Other types methods for the API 
	 */
	
}

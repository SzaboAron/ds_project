package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Review;
import entities.User;
import services.ReviewService;

@RestController
@RequestMapping("/api/reviews")
public class ReviewController {

	ReviewService reviewService;
	
	@Autowired
	public ReviewController(ReviewService reviewService){
		this.reviewService = reviewService;
	}
	
	/**
	 * Methods for the API
	 */
	
	@ResponseBody
	@RequestMapping(value = "/getall", method=RequestMethod.GET)
	public List<Review> getAll(){
		return reviewService.getAllReviews();
	}
	
	@ResponseBody
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Review addReview(@RequestParam String review){
		Review r = new Review();
		r.setReview(review);
		return reviewService.addReview(r);
	}
	
	@ResponseBody
	@RequestMapping(value="/getbyid",method=RequestMethod.GET)
	public List<Review> getById(User user){
		return reviewService.getReviewById(user);
	}
	
	@ResponseBody
	@RequestMapping(value="/getbyusername",method=RequestMethod.GET)
	public List<Review> getByUserName(User user){
		return reviewService.getReviewByUserName(user);
	}
}

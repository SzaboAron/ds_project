package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Product;
import services.ProductService;

@RestController
@RequestMapping("/api/products")
public class ProductController {
	
	private ProductService productService;
	
	@Autowired
	public ProductController(ProductService productService){
		this.productService = productService;
	}
	
	/**
	 * Methods for the API
	 */
	
	@ResponseBody
	@RequestMapping(value = "/getall", method=RequestMethod.GET)
	public List<Product> getAll(){
		return productService.getAllProducts();
	}
	
	@ResponseBody
	@RequestMapping(value = "/add", method=RequestMethod.POST)
	public boolean addProduct(@RequestParam int inStock, @RequestParam float price, @RequestParam String productName){
		Product product = new Product();
		product.setInStock(inStock);
		product.setPrice(price);
		product.setProductName(productName);
		productService.addProduct(product);
		return true;
	}
	
}

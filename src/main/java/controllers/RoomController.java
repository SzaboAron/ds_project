package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Room;
import services.RoomService;

@RestController
@RequestMapping("/api/rooms")
public class RoomController {

	private RoomService roomService;
	
	@Autowired
	public RoomController(RoomService roomService) {
		this.roomService = roomService;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getall", method=RequestMethod.GET)
    public List<Room> getRooms() {
        return roomService.getAllRooms(); 
    }
	
	@ResponseBody
	@RequestMapping(value = "/getbyroomnr",method=RequestMethod.GET)
    public Room getRoom(Room room) {
        return roomService.getByRoomNr(room);
    }
	
	@ResponseBody
	@RequestMapping(value = "/add",method=RequestMethod.POST)
	public void postRoom(@RequestBody Room room){
		roomService.addRoom(room);
	}
	
	@ResponseBody
	@RequestMapping(value = "/update",method=RequestMethod.POST)
	public void updateRooms(Room room){
		roomService.update(room);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deletebyroomnr",method=RequestMethod.POST)
	public void deleteByRoomNr(Room room){
		roomService.deleteByRoomNr(room);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deletebyid",method=RequestMethod.POST)
	public Long updateByRoomNr(Room room){
		return roomService.deleteByRoomId(room);
	}
}

package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Booking;
import services.BookingService;

@RestController
@RequestMapping("/api/bookings")
public class BookingController {
	
	
	private BookingService bookingService;
	
	@Autowired
	public BookingController(BookingService bookingService) {
		this.bookingService = bookingService;
	}
	
	/**
	 * Methods for the API
	 */
	
	@ResponseBody
	@RequestMapping(value = "/getall", method=RequestMethod.GET)
	public List<Booking> getAll(){
		return bookingService.getAll();
	}
	
	@ResponseBody
	@RequestMapping(value = "/add", method=RequestMethod.POST)
	public Booking addBooking(@RequestParam String startingDate,@RequestParam String endDate,@RequestParam int roomNr){
		return bookingService.add(startingDate, endDate, roomNr);
	}
	
	@ResponseBody
	@RequestMapping(value = "/update", method=RequestMethod.POST)
	public Booking updateBooking(Booking booking){
		return bookingService.update(booking);
	}
	
}

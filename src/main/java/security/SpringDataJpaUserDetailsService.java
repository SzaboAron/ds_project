package security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import entities.User;
import repositories.UserDao;

@Component
public class SpringDataJpaUserDetailsService implements UserDetailsService {

	private final UserDao userDao;
	
	@Autowired
	public SpringDataJpaUserDetailsService(UserDao userDao) {
		this.userDao = userDao;
	}
	
	@Override
	public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
		User user = this.userDao.findByUserName(arg0);
		System.out.println("Trying to log in with username:"+arg0);
		if (user == null) return null;
		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(),
				AuthorityUtils.createAuthorityList(user.getRoles()));
	}
	
	
}

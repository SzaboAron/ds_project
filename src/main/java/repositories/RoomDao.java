package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import entities.Room;

@Transactional
public interface RoomDao extends JpaRepository<Room, Long>{

	Room findByRoomNr(int roomNr);
	List<Room> findByRoomType(String type);
	Room findByIdrooms(Long id);
	Long deleteByRoomNr(int roomNr);
	Long deleteByIdrooms(Long id);
	
}

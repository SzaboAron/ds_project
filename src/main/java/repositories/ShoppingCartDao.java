package repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import entities.ShoppingCart;
import entities.User;

public interface ShoppingCartDao extends JpaRepository<ShoppingCart, Long> {
	
	ShoppingCart findByUser(User user);
	ShoppingCart findById(Long id);
}

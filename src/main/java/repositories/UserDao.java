package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import entities.User;

@Transactional
public interface UserDao extends JpaRepository<User, Long> {
	
	User findByUserName(String userName);
	User findById(Long id);
	List<User> findAll();
	User deleteByUserName(String userName);
	User deleteById(Long id);
	
}

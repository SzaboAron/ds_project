package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import entities.Booking;
import entities.Room;
import entities.User;

@Transactional
public interface BookingDao extends JpaRepository<Booking, Long> {

	List<Booking> findByClient(User client);
	List<Booking> findByRoom(Room room);
	Booking findByIdbookings(Long id);
	
}

package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import entities.Order;
import entities.Product;
import entities.User;

@Transactional
public interface OrderDao extends JpaRepository<Order, Long>{
	
	List<Order> findByProduct(Product product);

}

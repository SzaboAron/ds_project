package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import entities.Product;

@Transactional
public interface ProductDao extends JpaRepository<Product, Long> {
	
	Product findByProductName(String productName);
	Product findByIdproducts(Long id);
	
}

package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import entities.Review;
import entities.User;

@Transactional
public interface ReviewDao extends JpaRepository<Review, Long>{
	
	List<Review> findByClient(User client);
	Review findByIdreviews(Long id);
	
}

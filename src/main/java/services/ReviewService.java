package services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import entities.Review;
import entities.User;
import repositories.ReviewDao;
import repositories.UserDao;

@Component
public class ReviewService {
	
	private ReviewDao reviewDao;
	private UserDao userDao;
	
	@Autowired
	public ReviewService(ReviewDao reviewDao, UserDao userDao) {
		this.reviewDao = reviewDao;
		this.userDao = userDao;
	}
	
	public List<Review> getAllReviews(){
		return reviewDao.findAll();
	}
	
	public Review addReview(Review review){
		if (review == null) return null;
		if (review.getReview().equals("")) return null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		review.setClient(userDao.findByUserName(auth.getName()));
		return reviewDao.save(review);
	}
	
	public List<Review> getReviewById(User user){
		if (user == null) return null;
		return reviewDao.findByClient(user);
	}
	
	public List<Review> getReviewByUserName(User user){
		if (user == null) return null;
		if (user.getUserName().equals("")) return null;
		return reviewDao.findByClient(userDao.findByUserName(user.getUserName()));
	}

}

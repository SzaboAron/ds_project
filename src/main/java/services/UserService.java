package services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import entities.User;
import repositories.UserDao;

@Component
public class UserService {

	private UserDao userDao;
	
	@Autowired
	public UserService(UserDao userDao){
		this.userDao = userDao;
	}
	
	public List<User> getAllUsers(){
		return userDao.findAll();
	}
	
	public User addUser(User user){
		if (user == null) return null;
	//	if (!user.getRoles().equals("ROLE_ADMIN") || !user.getRoles().equals("ROLE_USER") ||
	//			user.getUserName().equals("")) return null;
		if (userDao.findByUserName(user.getUserName()) != null) return null;
		userDao.save(user);
		return user;
	}
	
	public User findByUserName(User user){
		if (user == null) return null;
		if (user.getUserName().equals("")) return null;
		return userDao.findByUserName(user.getUserName());
	}
	
	public User getCurrentUser(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return userDao.findByUserName(auth.getName());
	}
	
	public User deleteByUsername(User user){
		if (user == null) return null;
		if (user.getUserName().equals("")) return null;
		return userDao.deleteByUserName(user.getUserName());
	}
	
	public boolean checkUserName(User user){
		User u = userDao.findByUserName(user.getUserName());
		if (u == null) return true;
		return false;
	}
	
	public User addClient(User user){
		if (user == null) return null;
		if (user.getUserName() == null || user.getFullName() == null || user.getPassword() == null) {
			System.out.println("Smthing is null.. "+user.getPassword());
			return null;
		}
		if (user.getPassword().equals("") || user.getUserName().equals("") || user.getFullName().equals("")) {
			System.out.println("Smthing is empty...");
			System.out.println(user.getPassword());
			return null;
		}
		user.setRoles("ROLE_USER");
		System.out.println(user);
		return userDao.save(user);
	}
}

package services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import entities.Room;
import repositories.RoomDao;

@Component
public class RoomService {

	private RoomDao roomDao;
	
	@Autowired
	public RoomService(RoomDao roomDao) {
		this.roomDao = roomDao;
	}
	
	public List<Room> getAllRooms(){
		return roomDao.findAll();
	}
	
	public Room addRoom(Room room){
		if (room == null) return null;
		//if (room.getRoomType().equals("") || room.getRoomNr() <= 0 ||
		//		room.getPricePerNight() <= 0) return null;
		System.out.println(room);
		return roomDao.save(room);
	}
	
	public Room getByRoomNr(Room room){
		if (room == null) return null;
		if (room.getRoomType().equals("") || room.getRoomNr() <= 0 ||
				room.getPricePerNight() <= 0) return null;
		
		return roomDao.findByRoomNr(room.getRoomNr());
	}
	
	public Room update(Room room){
		if (room == null) return null;
		if (room.getRoomType().equals("") || room.getRoomNr() <= 0 ||
				room.getPricePerNight() <= 0 || room.getId() < 0) return null;
		return roomDao.save(room);
	}
	
	public Room updateByRoomNr(Room room){
		if (room == null) return null;
		if (room.getRoomType().equals("") || room.getRoomNr() <= 0 ||
				room.getPricePerNight() <= 0) return null;
		Room temp = roomDao.findByRoomNr(room.getRoomNr());
		temp.setPricePerNight(room.getPricePerNight());
		temp.setRoomNr(room.getRoomNr());
		temp.setRoomType(room.getRoomType());
		return roomDao.save(temp);
	}
	
	public Long deleteByRoomNr(Room room){
		if (room == null) return null;
		return roomDao.deleteByRoomNr(room.getRoomNr());
	}
	
	public Long deleteByRoomId(Room room){
		if (room == null) return null;
		if (room.getId() == null) return null;
		if (room.getId() < 0) return null;
		return roomDao.deleteByIdrooms(room.getId());
	}
	
	
}

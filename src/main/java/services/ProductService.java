package services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import entities.Product;
import repositories.ProductDao;

@Component
public class ProductService {

	private ProductDao productDao;
	
	@Autowired
	public ProductService(ProductDao productDao) {
		this.productDao = productDao;
	}
	
	public List<Product> getAllProducts(){
		return productDao.findAll();
	}
	
	public Product addProduct(Product product){
		if (product == null) return null;
		if (product.getProductName().equals("") || product.getPrice() == 0) return null;
		productDao.save(product);
		return product;
	}
}

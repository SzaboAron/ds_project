package services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import entities.ShoppingCart;
import entities.User;
import repositories.ShoppingCartDao;
import repositories.UserDao;

@Component
public class ShoppingCartService {

	ShoppingCartDao shoppingCartDao;
	UserDao userDao;
	
	@Autowired
	public ShoppingCartService(ShoppingCartDao shoppingCartDao, UserDao userDao){
		this.shoppingCartDao = shoppingCartDao;
		this.userDao = userDao;
	}
	
	public List<ShoppingCart> getAllCarts(){
		return shoppingCartDao.findAll();
	}
	
	public ShoppingCart getByClient(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return shoppingCartDao.findByUser(userDao.findByUserName(auth.getName()));
	}
	
	public ShoppingCart addCart(ShoppingCart cart){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		cart.setUser(userDao.findByUserName(auth.getName()));
		return shoppingCartDao.save(cart);
	}
}

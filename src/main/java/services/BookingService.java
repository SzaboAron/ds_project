package services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import entities.Booking;
import repositories.BookingDao;
import repositories.RoomDao;
import repositories.UserDao;

@Component
public class BookingService {

	private BookingDao bookingDao;
	private UserDao userDao;
	private RoomDao roomDao;
	
	@Autowired
	public BookingService(BookingDao bookingDao, UserDao userDao, RoomDao roomDao){
		this.bookingDao = bookingDao;
		this.userDao = userDao;
		this.roomDao = roomDao;
	}
	
	@PreAuthorize("hasRole('ROLE_USER')")
	public Booking add(String startingDate, String endDate, int roomNr){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
       
		
		Booking booking = new Booking();
		
		try{
			 Date date = formatter.parse(startingDate);
			 booking.setStartingDate(date);
			 date = formatter.parse(endDate);
			 booking.setEndDate(date);
		}catch(ParseException ex){
			return null;
		}
		
		booking.setRoom(roomDao.findByRoomNr(roomNr));
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		booking.setClinet(userDao.findByUserName(auth.getName()));
		bookingDao.save(booking);
		return booking;
	}
	
	public List<Booking> getAll(){
		return bookingDao.findAll();
	}
	
	public Booking update(Booking booking){
		if (booking == null) return null;
		if (booking.getClinet() == null || booking.getEndDate().equals("") 
				|| booking.getStartingDate().equals("") || booking.getIdbookings()<0 ) return null;
		bookingDao.save(booking);
		return booking;
	}
	
}

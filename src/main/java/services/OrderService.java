package services;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import entities.Order;
import repositories.OrderDao;
import repositories.ProductDao;
import repositories.ShoppingCartDao;
import repositories.UserDao;

@Component
public class OrderService {
	
	private OrderDao orderDao;
	private UserDao userDao;
	private ShoppingCartDao shoppingCartDao;
	private ProductDao productDao;
	
	@Autowired
	public OrderService(OrderDao orderDao, UserDao userDao,ShoppingCartDao shoppingCartDao, ProductDao productDao){
		this.orderDao = orderDao;
		this.userDao = userDao;
		this.shoppingCartDao = shoppingCartDao;
		this.productDao = productDao;
	}
	
	public List<Order> getAllOrders(){
		return orderDao.findAll();
	}
	
	public Order addOrder(Long productId, Long cartId, int quantity){
		Order order = new Order();
		order.setQuantity(quantity);
		order.setProduct(productDao.findByIdproducts(productId));
		order.setShoppingCart(shoppingCartDao.findById(cartId));
		System.out.println("Shopping cart is "+order.getShoppingCart());
		System.out.println("Shopping cart should be : "+cartId);
		return orderDao.save(order);
	
	}
	
}
